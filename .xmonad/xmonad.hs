import XMonad
import Data.Monoid
import Data.Ratio
import System.Exit

import XMonad.Hooks.EwmhDesktops (fullscreenEventHook) 
import XMonad.Hooks.ManageDocks (avoidStruts, docks, docksEventHook)
import XMonad.Hooks.ManageHelpers (doRectFloat, isFullscreen, doFullFloat)
import XMonad.Hooks.SetWMName
import XMonad.Layout.Spacing (spacingWithEdge)
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.SpawnOnce
import XMonad.Util.Run (spawnPipe)

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

------------------------------------------------------------------------

myTerminal           = "urxvt -e fish" :: String
  
myBrowser            = "librewolf" :: String

myWorkspaces         = ["1","2","3","4","5","6","7","8","9"] :: [String]

myLogHook            = return ()

myManageHook         = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore 
    , title =? "Oracle VM VirtualBox Manager"     --> doRectFloat (W.RationalRect (1 % 4) (1 % 4) (1 % 2) (1 % 2))
    , title =? "virt-manager"     --> doRectFloat (W.RationalRect (1 % 4) (1 % 4) (1 % 2) (1 % 2))
    , isFullscreen -->  doFullFloat]

myStartupHook = do
        let hooks = ["picom"
                    , "$HOME/.fehbg"
--                    , "nitrogen --restore"
                    , "emacs --daemon &"
                    , "ibus-daemon -drvxR"]
        mapM spawnOnce hooks
        setWMName "LG3D"

------------------------------------------------------------------------

myKeys :: [(String, X ())]
myKeys = 
    [ ("M-S-<Return>", spawn myTerminal)
    , ("M-p", spawn "dmenu_run -fn 'SourceCodePro:size=10' -p 'Search ' -bw 2")
    , ("M-f", spawn myBrowser)
    , ("M-e", spawn "emacsclient -c -a emacs")
    , ("M-S-c", kill)

    , ("M-<Space>", sendMessage NextLayout)
    , ("M-n", refresh)

    , ("<XF86AudioLowerVolume>", spawn "amixer -c0 set Master 5%- unmute")
    , ("<XF86AudioRaiseVolume>", spawn "amixer -c0 set Master 5%+ unmute")
    , ("<XF86AudioNext>", spawn "xbacklight -inc 20")
    , ("<XF86AudioPrev>", spawn "xbacklight -dec 20")

    , ("M-Tab", windows W.focusDown)
    , ("M-j", windows W.focusDown)
    , ("M-k", windows W.focusUp  )
    , ("M-m", windows W.focusMaster  )
    , ("M-<Return>", windows W.swapMaster)
    , ("M-S-j", windows W.swapDown  )
    , ("M-S-k", windows W.swapUp    )

    , ("M-h", sendMessage Shrink)
    , ("M-l", sendMessage Expand)

    , ("M-t", withFocused $ windows . W.sink)

    , ("M-,", sendMessage (IncMasterN 1))
    , ("M-.", sendMessage (IncMasterN (-1)))

    , ("M-q", io $ exitWith ExitSuccess)
    , ("M-r", spawn "killall xmobar; xmonad --recompile; xmonad --restart")
    ]

------------------------------------------------------------------------

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

------------------------------------------------------------------------

myLayout = avoidStruts $ tiled ||| Mirror tiled ||| Full
  where
     tiled   = spacingWithEdge 5 $ Tall nmaster delta ratio
     nmaster = 1
     ratio   = 1/2
     delta   = 3/100

------------------------------------------------------------------------

main = do
  xmproc <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc"
  xmonad $ docks def {
    borderWidth        = 2,
    modMask            = mod4Mask,
    normalBorderColor  = "#4d4d4d",
    focusedBorderColor = "#1d3b49",
    focusFollowsMouse  = True,
    clickJustFocuses   = False,
      
    terminal           = myTerminal,
    workspaces         = myWorkspaces,
    mouseBindings      = myMouseBindings,
      
    layoutHook         = myLayout,
    manageHook         = myManageHook,
    handleEventHook    = docksEventHook <+> fullscreenEventHook,
    logHook            = myLogHook,
    startupHook        = myStartupHook
    } `additionalKeysP` myKeys

