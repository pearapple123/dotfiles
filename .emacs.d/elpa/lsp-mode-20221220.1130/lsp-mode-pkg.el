(define-package "lsp-mode" "20221220.1130" "LSP mode"
  '((emacs "26.3")
    (dash "2.18.0")
    (f "0.20.0")
    (ht "2.3")
    (spinner "1.7.3")
    (markdown-mode "2.3")
    (lv "0")
    (eldoc "1.11"))
  :commit "4cd7b9d4d7da45b544ab35a701bcd785dd20c2ed" :authors
  '(("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainer
  '("Vibhav Pant, Fangrui Song, Ivan Yonchovski")
  :keywords
  '("languages")
  :url "https://github.com/emacs-lsp/lsp-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
