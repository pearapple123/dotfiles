//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"BAT0:", "cat /sys/class/power_supply/BAT0/charge_now | awk '{ printf \"%0.1f%\", $1*100/3933000 }'", 15, 0},
	{"", "bash /home/tarun/bat_estimate.sh | awk '{ print $a }'", 15, 0},
	{"Storage:", "df | grep 'sda3' | awk '{ printf \"%0.2f%\", $3*100/$2 }'", 		30, 		0},
	{"", "date '+%a %d %b %H:%M'",						5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
