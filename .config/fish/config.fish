set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $fish_user_paths

set fish_greeting

#set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

#vi mode
function fish_user_key_bindings
    fish_vi_key_bindings
end

#Aliases
alias ls='exa -lha'
alias la='ranger'
alias vim='nvim'
alias cat='bat'
alias grep='grep --color=auto'

alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

#Pacman-based
#alias update="sudo pacman -Syu"
#alias search="pacman -Ss"
#alias orphan="sudo pacman -Rns (pacman -Qtdq)"
#alias auraup="sudo aura -Akuax"

#Xbps-based
alias update="sudo xbps-install -Su"
alias search="xbps-query -Rs"
alias remove="sudo xbps-remove"

starship init fish | source
