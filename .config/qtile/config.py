# imports
import subprocess
import os
from typing import List
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

# vars
mod = "mod4"
terminal = "st"
browser = "qutebrowser"
bar_bgcolor = "#282c34"
bar_fgcolor = "#ffffff"
my_font = "JetBrainsMono"
wmname = "LG3D"
# vars for quick powerline customisation
pline_padding = -12.5 
pline_size = 65

# keybinds
keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn("st -e fish"), desc="Launch terminal"),
    Key([mod], "f", lazy.spawn(browser), desc="Launch browser"),
    Key([mod], "p", lazy.spawn("dmenu_run -g 3 -l 10 -bw 2"), desc="Launch dmenu"),
    Key([mod], "e", lazy.spawn("emacsclient -c -a emacs"), desc="Launch emacs"),
    Key([mod, "shift"], "p", lazy.spawn('.local/bin/aw'), desc="Launch ArchWiki dmenu"),

    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
]

# Groups
groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
    ])

# layouts
layouts = [
    layout.Columns(border_focus_stack='#190b0b'),
    layout.Max(),
]

# qtile bar settings
widget_defaults = dict(
    font=my_font+' Bold',
    fontsize=12,
    padding=3,
    background=bar_bgcolor
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
               widget.Image(
                    filename = "~/.config/qtile/python.png",
                    scale = "True",
                    margin=2.25,
                    ),

                widget.GroupBox(
                    highlight_method='block',
                    rounded = False,
                    this_current_screen_border='924441',
                    this_screen_border='924441'
                    ),

                widget.Prompt(),

                widget.WindowName(
                    ),

                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                    ),
                
               widget.TextBox(
                    text="",
                    fontsize=pline_size,
                    padding=pline_padding,
                    background=bar_bgcolor,
                    foreground=bar_fgcolor,
                    ),
                
                widget.GenPollText(
                    update_interval=60,
                    func=lambda: subprocess.check_output(os.path.expanduser("/home/tarun/.config/qtile/pacupdate.sh")).decode('utf-8').strip('\n'),
                    fmt='{} updates',
                    padding=5,
                    background=bar_fgcolor,
                    foreground=bar_bgcolor,
                ),
                
                widget.TextBox(
                    text="",
                    fontsize=pline_size,
                    padding=pline_padding,
                    background=bar_fgcolor,
                    foreground=bar_bgcolor,
                    ),
 
                widget.GenPollText(
                    update_interval=10,
                    func=lambda: subprocess.check_output(os.path.expanduser("/home/tarun/.config/qtile/packagenum.sh")).decode('utf-8').strip('\n'),
                    fmt='{} packages',
                    background=bar_bgcolor,
                    foreground=bar_fgcolor,
                    padding=5,
                    ),

                widget.TextBox(
                    text="",
                    fontsize=pline_size,
                    padding=pline_padding,
                    foreground=bar_fgcolor,
                    background=bar_bgcolor,
                    ),

                widget.Battery(
                    show_short_text = False,
                    format = 'BAT {char}{percent:2.0%} {hour:d}:{min:02d}',
                    full_char = "",
                    padding = 5,
                    background=bar_fgcolor,
                    foreground=bar_bgcolor,
                    discharge_char="↓",
                    charge_char="↑",
                    ),

                widget.TextBox(
                    text="",
                    fontsize=pline_size,
                    foreground=bar_bgcolor,
                    padding=pline_padding,
                    background=bar_fgcolor
                    ),

                widget.Clock(format='%a %d/%m/%Y %H:%M',
                    foreground = bar_fgcolor,
                    background = bar_bgcolor,
                    padding=5,
                    ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# autostart apps
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

