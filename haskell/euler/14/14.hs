import System.Environment (getArgs)
import Control.Monad (liftM)

collatz :: Int -> [Int]
collatz 1 = [1]
collatz n
  | even n = n : collatz (div n 2)
  | otherwise = n : collatz(3*n + 1)

main :: IO ()
main = do
  nUpper <- liftM (read . flip (!!) 0) getArgs          -- liftM kinda allows you to apply functions that usu. take non-monadic inputs, to monadic inputs
                                                        -- e.g. apply flip (!!) 0 to IO Strings
  print $ maximum $ map (length . collatz) [1..nUpper]
