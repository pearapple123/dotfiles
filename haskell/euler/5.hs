evenDiv1To20 :: Integer
evenDiv1To20 = foldl1 (\x acc -> if all (\n -> mod x n == 0) [1..10] then x else acc) [1..]