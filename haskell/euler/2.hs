-- Let bindings
in_range min max x =
  let in_lower_bound = min <= x
      in_upper_bound = max >= x
  in in_lower_bound && in_upper_bound

-- where bindings
in_range' min max x = ilb && iub
  where
    ilb = min <= x
    iub = max >= x
