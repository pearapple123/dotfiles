-- Higher order funcs
app :: (a -> b) -> a -> b
app f x = f x

add1 :: Int -> Int
add1 = (\x -> x+1)

-- Anonymous funcs
-- not defined with a name
-- (\<args> -> <expr>)
-- funcs are just values
app2 :: (Num a) => a -> a  
app2 x = (\x -> x+1) x -- Application of anon funcs

-- Important higher order funcs
-- map (\x -> x+1)
-- filter <condition via anon func>
