import Data.List(union)

sumOfMultsBelow lim = sum $ filter (\x -> mod x 3 == 0 || mod x 5 == 0) [1..lim]

sumOfMultsBelow' lim = aux lim 0
  where
    aux x acc
      | x <= 1 = acc
      | mod x 3 == 0 || mod x 5 == 0 = aux (x-1) (x+acc)
      | otherwise = aux (x-1) acc

sumOfMultsBelow'' lim = sum $ union [3,6..lim] [5,10..lim]
