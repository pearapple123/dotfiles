f :: a -> b -> c -> d
-- is really
-- f :: a -> (b -> (c -> d))

add :: Int -> Int -> Int
add x y = x + y
-- OR add x = (\y -> x+y)
-- OR add = (\x -> (\y -> x+y))

-- add 1 = (1 -> (\y -> 1+y))

doubleList = map (\x -> x*2)
-- doesn't require explicit arg since it implicitly gets one
