-- Recursion
fac :: Integer -> Integer
fac n
  | n <= 1 = 1
  | otherwise = n * fac (n-1)

-- More declarative
fac' =
  if n <= then
    1
  else
    n * fac (n-1)

-- Pattern matching
is_zero :: Integer -> Bool
is_zero 0 = True
is_zero _ = False

-- Accumulators
-- using a secondary func `aux` within the original `fac` function
fac2 n = aux n 1
  where
    aux n acc
      | n <= 1 = acc
      | otherwise = aux (n-1) (n*acc)
{- 'Tail-recursive function'
 - Clever compilers can convert these into while true loops, avoiding stack overflow errors, as it doesn't add another stack frame on stack for each recursive call
-}
