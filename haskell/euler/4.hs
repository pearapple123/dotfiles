-- Lists

-- Import list funcs
import Data.List

-- Cons constructor
consList = 1 : 2 : 3 : 4 : 5 : [] -- [1,2,3,4,5]

-- Generating a list
-- Make a list from x to y
asc :: Int -> Int -> [Int]
asc x y
  | x > y = []
  | x <= y = x : asc (x+1) y

-- LIST FUNCS
-- head -> first elem
-- tail -> drops first elem
-- length -> length of list
-- init -> drops last elem
-- last -> last elem
--
-- and -> only returns True if all elems are True
-- or -> returns True if >= 1 elems are True

-- List Patterns
sum' :: [Int] -> Int
sum [] = 0
sum (x:xs) = x + sum xs

evens' :: [Int] -> Int
evens' [] = []
evens' (x:xs)
  | mod x 2 == 0 = x : evens xs -- include if fulfilling criteria via cons
  | otherwise = evens xs        -- don't include otherwise
{- Alt:
 - evens' a = [x | x <- a, mod x 2 == 0]
 -}

-- Tuples can consist of elems of diff types, and can have more than two elems
