triplets = maximum $ map (\(x,y,z) -> x*y*z) [(m*m - n*n, 2*m*n, m*m + n*n) | m <- [1..33],n <- [1..33], m > n, m*m + m*n == 500]
