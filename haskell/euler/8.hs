-- Func composition
-- (.) :: (b -> c) -> (a -> b) -> a -> c => two funcs and a value return a value
-- Thus, (f . g) == (\x -> f (g x))

-- descSort = reverse . sort

-- Equal to descSort = (\x -> reverse (sort x))
-- which equals descSort x = reverse $ sort x

map2D :: (a -> b) -> [[a]] -> [[b]]
map2D = map . map -- apply func to 2D list
