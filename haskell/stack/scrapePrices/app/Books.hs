{-# LANGUAGE OverloadedStrings #-}
-- This scraper gets the titles, prices and ratings of all books on BooksToScrape
-- For some reason, I'm not getting any output when I scrape for the title attr or the text in each <a>
-- so it's just hrefs for the titles for now

module Books where

import Text.HTML.Scalpel
import Data.List.Split (splitOn)
import Data.List (sortOn)
import Data.Maybe (maybeToList)

data Entry = Entry {entName :: String
                   , entPrice :: Float
                   , entRate :: Int
                   } deriving Eq

instance Show Entry where
  show (Entry n p r) = "Name: " ++ n ++ "\nPrice: " ++ show p ++ "\nRating: " ++ show r ++ "/5\n"

allItems :: Scraper String [Entry]
allItems = chroots ("article" @: [hasClass "product_pod"]) $ do
    p <- text $ "p" @: [hasClass "price_color"]
    t <- attr "href" "a"
    star <- attr "class" $ "p" @: [hasClass "star-rating"]
    let fp = read $ flip (!!) 1 $ splitOn "£" p
    let fStar = drop 12 star
    return $ Entry t fp $ r fStar
      where
        r f = case f of
          "One" -> 1
          "Two" -> 2
          "Three" -> 3
          "Four" -> 4
          "Five" -> 5

scrapePage :: Int -> IO [Entry]
scrapePage num =
  concat . maybeToList <$> scrapeURL ("https://books.toscrape.com/catalogue/page-" ++ show num ++ ".html") allItems

main :: IO ()
main = do
  ents <- concat <$> mapM scrapePage [1..10]
  let process = filter (\e -> entRate e == 5) . sortOn entPrice
  print $ process ents
