{-# LANGUAGE OverloadedStrings #-}
module Yugioh where

import Text.HTML.Scalpel

main :: IO ()
main = do
  passwd <- scrapeURL "https://yugipedia.com/wiki/Thousand_Dragon" getPasswd
  maybe (putStrLn "No passwd") print passwd

getPasswd :: Scraper String Int
getPasswd = chroots ("table" @: [hasClass "innertable"]) $ do
  passwd <- attr "a" "title"
  return $ read passwd
