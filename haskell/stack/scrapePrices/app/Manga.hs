{-# LANGUAGE OverloadedStrings #-}
-- A scraper that gets manga of certain tags from Baka Manga Updates
-- Getting ratings as well seems next to impossible

module Manga where

import Text.HTML.Scalpel
import Data.List (isInfixOf,nub,sortOn)
import Control.Monad (guard)
import Data.List.Split

type Tag = String

data Manga = Manga { mTitle :: String
                   , mTags :: [String]
                   } deriving Eq

instance Show Manga where
  show (Manga t ts) = "Name: " ++ t ++ "\nTags: " ++ show ts ++ "\n"


inclTags :: [Tag]
inclTags = []
--inclTags = ["Psychological","Romance"]

scrapeF :: Int -> IO [Manga]
scrapeF n =
  concat <$> scrapeURL ("https://www.mangaupdates.com/series.html?page=" ++ show n ++ "&perpage=100") scrapeTitles

scrapeTitles :: Scraper String [Manga]
scrapeTitles = chroots ("div" @: [hasClass "d-flex"]) $ do
  t <- text $ "b"
  tags <- attr "title" $ "a" @: ["style" @= "text-decoration: none"]
  guard $ not $ isInfixOf ['a'..'z'] t                                    -- cause ratings have the same <b> tag as titles, this bit disambiguates
  let fTags = splitOn ", " tags
  return $ Manga t fTags

process = filter (isSubsetOf inclTags . mTags) . sortOn mTitle . nub
  where isSubsetOf xs ys = all (`elem` ys) xs

main :: IO ()
main = concat <$> mapM scrapeF [1..1] >>= mapM_ (print) . process
