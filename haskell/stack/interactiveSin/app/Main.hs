module Main where

import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo
import System.Environment (getArgs)
import Control.Monad (liftM)

main :: IO ()
main = do
  args <- liftM (map (read :: String -> Double) . take 2) getArgs
  let (a,b) = (args !! 0, args !! 1) :: (Double,Double)
  toFile def "sine.png" $ do
    layout_title .= "My Sine"
    let xs = [0,2..720] :: [Double]
    let transformxs = map (flip (-) b . (*) a) xs
    plot (line "" [[(x,y)
                 | (x,y) <- zip xs $ map (sin . (*) (pi/180)) transformxs]])
