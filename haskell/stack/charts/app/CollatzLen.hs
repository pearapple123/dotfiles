module CollatzLen where

import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo

collatz :: Int -> [Int]
collatz 1 = [1]
collatz n
  | even n = n : collatz (div n 2)
  | otherwise = n : collatz (3*n + 1)

main = toFile def "coll_len.png" $ do
  layout_title .= "Collatz Length"
  let xs = [1..100]
  plot (line "" [[(x,y)
                 | (x,y) <- zip xs $ map (length . collatz) xs]])
