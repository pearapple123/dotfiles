module Main where

import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo
import Control.Monad.Random.Class
import Lib (xvals)

main = do
  rands <- (getRandoms :: MonadRandom m => m [Double])
  toFile def "chart.png" $ do
  layout_title .= "My chart"
  layout_x_axis . laxis_override .= axisGridHide
  let noise = filter (>0.8) rands
  let xs = xvals 20 0.0 10.0
  let ys = zipWith (*) xs noise
  plot (points "My points" [(x,y) | (x,y) <- zip xs ys])
