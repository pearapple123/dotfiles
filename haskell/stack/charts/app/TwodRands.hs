module TwodRands where

import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo
import Control.Monad (liftM2)
import Control.Monad.Random ()
import Control.Monad.Random.Class (getRandoms, MonadRandom)

upperLim :: Int
upperLim = 1000

numPts :: Int
numPts = 10000

main :: IO ()
main = do
  let getRs = map (*100) <$> take numPts <$> (getRandoms :: MonadRandom m => m [Double])
  pts <- liftM2 zip getRs getRs
  toFile def "2drand.png" $ do
    layout_title .= "Plotting 2D rands"
    plot (points "" pts)
