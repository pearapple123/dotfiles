module Lib
    ( gaussian
    , laplace
    , xvals
    , deriv
    , toCumu
    ) where

gaussian :: Double -> Double -> Double -> Double
gaussian mean std x = (1/std*sqrt(2*pi))*exp(-0.5*((x-mean)/std)**2)

laplace :: Double -> Double -> Double -> Double
laplace b mean x = (1/(2*b)) * exp(-(abs $ x - mean)/b)

xvals :: Int -> Double -> Double -> [Double]
xvals steps x_0 x_n = [x_0, x_0 + dt .. x_n]
  where
    dt = (/) (x_n - x_0) (fromIntegral steps)

deriv :: (Double -> Double) -> Double -> Double
deriv func x = (/) (func (x + dt) - func(x)) dt
  where dt = 0.1 :: Double

toCumu :: Num a => [a] -> [a]
toCumu ns = scanl1 (\x acc -> x + acc) ns
