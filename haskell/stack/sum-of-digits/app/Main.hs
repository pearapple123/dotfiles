module Main where

import Data.List
import NumMethods

sumEq :: Int -> Int -> [Int]
sumEq lim x = [num | num <- [1..lim], sumOfDigits num == x]

fromDigits :: [Int] -> Int
fromDigits xs = aux xs 0
    where aux [] acc = acc
          aux (x:xs) acc  = aux xs ((acc * 10) + x)

removePermReps :: Int -> [Int]
removePermReps y = map fromDigits $ nub $ map (\x -> sort $ digs x) $ sumEq 100000 y

main :: IO ()
main = putStrLn $ show $ length $ sumEq 100000 39
