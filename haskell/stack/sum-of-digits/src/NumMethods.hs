module NumMethods
    ( digs, sumOfDigits
    ) where

digs :: Integral x => x -> [x]
digs 0 = []
digs x = digs (x `div` 10) ++ [x `mod` 10]

sumOfDigits :: Int -> Int
sumOfDigits = sum . digs
