{-# LANGUAGE OverloadedStrings #-}
module ScottySite (scottySite) where

import Web.Scotty
import Data.Text.Lazy.Encoding 
--import Templates.Login

scottySite :: IO ()
scottySite = scotty 3000 $ do
    get "/" $ file "static/home.html"
    get "/search" $ file "static/search.html"
    post "/getresults" $ do
        b <- body
        text $ decodeUtf8 b

--    get "/login" $ html $ renderHtml loginForm 
--    post "/create_user" $ do


{-    get "/" $ text "bar" 

    get "/params" $ do
        txt <- param "prm"
        html $ mconcat ["<h1>",txt,"</h1>"]

  get "/form" $ do
        html $ mconcat ["<form method=POST action=\"readbody\">"
                       ,"<input type=text name=something>"
                       ,"<input type=submit>"
                       ,"</form>"
                       ]

    post "/readbody" $ do
        b <- body
        text $ decodeUtf8 b
-}