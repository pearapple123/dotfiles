{-# LANGUAGE OverloadedStrings  #-}
module Templates.Home (homeHtml) where

import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A 

homeHtml :: H.Html
homeHtml = H.docTypeHtml $ do
             H.head $ do
                 H.title "My Scotty Website"
                 H.link H.! A.rel "stylesheet" H.! A.type_ "text/css" H.! A.href "src/static/styles.css"
             H.body $ do
                 H.h1 "Lorem ipsum"
                 H.p "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
