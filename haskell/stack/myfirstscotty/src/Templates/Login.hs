{-# LANGUAGE OverloadedStrings  #-}
module Templates.Login (loginForm) where

import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

loginForm = H.docTypeHtml $ do
    H.head $ do
        H.title "Login Form"
    H.body $ do
        H.h2 "Login"
        H.form H.! A.action "createUser" $ do
            H.input H.! A.type_ "text" H.! A.name "username"
            H.input H.! A.type_ "email" H.! A.name "email"
            H.input H.! A.type_ "submit" H.! A.name "submit"