module CritVal
    ( getSpmanCritVal
    , getPpmccCritVal
    ) where

import qualified Data.List as L 
import Correl (spman)

-- get list of permutations of 1 to n, then calc Spearman's rank coeff for each permutation
-- sort list of spman in descending order, then take from this list a number = significance * length of list
-- the last value in the taken list is the critical value
-- horribly slow and imprecise
getSpmanCritVal :: (RealFrac a, Ord b, Floating b, Enum b) => a -> b -> b
getSpmanCritVal alpha n = last $ take (round $ alpha * fromIntegral (length getSpmanVals)) getSpmanVals
  where
    getSpmanVals = reverse $ L.sort $ map (abs . spman . zip [1..n]) $ L.permutations [1..n]

getPpmccCritVal :: RealFrac a => a -> Int -> a
getPpmccCritVal alpha n = 1 
  --where
 --   randPoints :: (Num a, Random a) => [(a,a)]
--    randPoints = take n [(x,y) | x <- randomRs (1,1000000) $ mkStdGen 1, y <- randomRs (1,1000000) $ mkStdGen 2]