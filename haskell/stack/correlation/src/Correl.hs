module Correl
    ( ppmcc
    , spman
    , sxx
    , sxy
    , syy
    ) where

sxx :: Floating a => ([a],[a]) -> a
sxx (xs,_) = sumxx - sum xs**2/fromIntegral (length xs)
  where
    sumxx = foldr (\x acc -> x*x + acc) 0 xs

syy :: Floating a => ([a],[a]) -> a
syy (_,ys) = sumyy - sum ys**2/fromIntegral (length ys)
  where
    sumyy = foldr (\y acc -> y*y + acc) 0 ys

sxy :: Floating a => ([a],[a]) -> a
sxy (xs,ys) = sumxy - sum xs*sum ys/fromIntegral (length xs)
  where sumxy = sum $ zipWith (*) xs ys

ppmcc :: Floating a => [(a,a)] -> a
ppmcc dset = sxy set / sqrt (sxx set * syy set)
  where
    set = unzip dset

spman :: (Floating a, Ord a) => [(a,a)] -> a 
spman = ppmcc . ununzip . ranks . unzip
  where
    ranks :: (Ord a, Num a) => ([a],[a]) -> ([a],[a])
    ranks (xs,ys) = (rank xs, rank ys)
      where rank xs = [fromIntegral $ (+) 1 $ length $ filter (>x) xs | x <- xs]

    ununzip :: ([k],[a]) -> [(k,a)]
    ununzip (xs,ys) = zip xs ys
