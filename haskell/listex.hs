-- elem' returns True if in list, False otherwise

{-
- elem' :: (Eq a) => a -> [a] -> Bool
- elem' a m = or [x == a | x <- m]
-}

elem' :: (Eq a) => a -> [a] -> Bool
elem' _ [] = False
elem' e (x:xs) = (e == x) || (elem' e xs)

-- nub' removes duplicates from list
nub' :: (Eq a) => [a] -> [a]
nub' [] = []
nub' (x:xs)
  | elem' x xs == False = x : nub' xs
  | otherwise = nub' xs 

-- isAsc returns True if list is in ascending order

head' :: [a] -> a
head' (x:xs) = x

null' :: (Eq a) => [a] -> Bool
null' x = x == []

isAsc :: [Int] -> Bool
isAsc (x:xs)
  | null' xs = True
  | x > head' xs = False
  | otherwise = isAsc xs

{-
 - isAsc [] = True
 - isAsc [x] = True
 - isAsc (x:y:xs) = (x <= y) && isAsc (y:xs)
-}

routes = [(1,2),(2,3),(3,2),(4,3),(4,5)] :: [(Int,Int)]

-- Solution
hasPath :: [(Int,Int)] -> Int -> Int -> Bool
hasPath [] x y = x == y
hasPath xs x y
  | x == y = True
  | otherwise = or [ hasPath xs' m y | (n,m) <- xs, n == x ]
    where xs' = [(n,m) | (n,m) <- xs, n /= x] -- search results where fst of all tuples in `routes` isn't x
