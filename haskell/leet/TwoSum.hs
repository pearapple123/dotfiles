import Control.Applicative (liftA2)
import Data.List (elemIndex,nub)

nums = [2,7,11,15]
tgt = 9

combs = nub $ concat $ map (\(x,y) -> map (flip elemIndex nums) [x,y]) $ filter (\(x,y) -> x+y==tgt) $ liftA2 (,) nums nums

