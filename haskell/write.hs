import Control.Monad.Writer hiding (Writer)  
  
logNumber :: Int -> WriterT [String] Int  
logNumber x = writer (x, ["Got number: " ++ show x])  
  
multWithLog :: WriterT [String] Int  
multWithLog = do  
    a <- logNumber 3  
    b <- logNumber 5  
    return (a*b)  
