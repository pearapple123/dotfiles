import qualified Data.Vector as V

nums = do
  let sqNumsM = V.generateM 10 (\i -> Just $ i*i)
  sqNums <- sqNumsM
  return $ mapM (\x -> Just $ round $ sqrt $ fromIntegral x) sqNums


