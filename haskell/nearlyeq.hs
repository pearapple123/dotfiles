import Data.List (intersect,length)

nearlyEq :: (Num a, Eq a) => Int -> [a] -> [a] -> Bool
nearlyEq precis xs ys = lenNearEq == length ys && lenNearEq == length xs
  where lenNearEq = (+) precis $ length $ intersect xs ys
