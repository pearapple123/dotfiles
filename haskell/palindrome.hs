module Main where

palindrome :: Int -> Bool
palindrome x = backwards x == show x
  where backwards = reverse . show

palindrome' :: Int -> Bool
palindrome' x
  | x %

main :: IO ()
main = do
  putStrLn $ show $ palindrome 121
  putStrLn $ show $ palindrome (-121)
  putStrLn $ show $ palindrome 10
