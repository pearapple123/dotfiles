import System.Random

nstep = 10000

movesPerStep :: Int -> [Bool]
movesPerStep n = take n $ randoms (mkStdGen 11)

rwalk ns = aux ns 0
  where
    aux [] _ = []
    aux (n:ns) acc
      | n == True = acc + 1 : aux ns (acc+1)
      | otherwise = acc - 1 : aux ns (acc-1)

dataset = zip [1..nstep] $ rwalk $ movesPerStep nstep
