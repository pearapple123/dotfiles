import Data.Monoid
import Data.Semigroup
import Data.List.NonEmpty (NonEmpty (..))

sum1 :: [Int] -> Int
sum1 (x:xs) = x + sum xs

sum2 :: [Int] -> Int
sum2 = foldl1 (\x acc -> x + acc)

sum3 :: Num a => [Sum a] -> Sum a
sum3 xs = sconcat $ mempty :| xs

elem1 :: Int -> [Int] -> Bool
elem1 _ [] = False 
elem1 x (y:ys)
  | x /= y = elem1 x ys
  | otherwise = True

elem2 :: Int -> [Int] -> Bool
elem2 y = foldl (\acc x -> if x == y then True else acc) False

elem3 :: Int -> [Int] -> Bool
elem3 x xs = or [y == x | y <- xs]

length1 :: [Int] -> Int
length1 = foldr (\x acc -> 1 + acc) 0

length2 :: [Int] -> Int
length2 [] = 0
length2 (x:xs) = 1 + length2 xs

-- pow x n has domain `n ∈ Z, x ∈ R`
pow :: Float -> Int -> Float
pow x n
  | n > 0 = foldl1 (*) $ replicate n x
  | n < 0 = foldl1 (*) $ replicate (-n) (1/x)
  | n == 0 = 1

pow2 :: Fractional a => a -> Int -> Product a
pow2 x n
  | n < 0 = mtimesDefault (-n) $ Product (1/x)
  | otherwise = mtimesDefault n $ Product x

map1 :: (a -> b) -> [a] -> [b]
map1 f [] = []
map1 f (x:xs) = f x : map1 f xs

map2 :: (a -> b) -> [a] -> [b]
map2 f xs = [f x | x <- xs]

map3 :: (a -> b) -> [a] -> [b]
map3 f xs = foldr (\x acc -> f x : acc) [] xs

fac :: (Num a, Eq a) => a -> a
fac 0 = 1
fac 1 = 1
fac n = n * fac (n-1)

sin' :: (Floating a, Eq a, Enum a) => a -> a
sin' x = sum $ take m [(-1)**n * x**(1+2*n)/fac (1+2*n) | n <- [0..]]
  where m = 10

cos' :: (Floating a, Eq a, Enum a) => a -> a
cos' x = sum $ take m [(-1)**n * x**(2*n)/fac (2*n) | n <- [0..]]
  where m = 10

tan' :: (Floating a, Eq a, Enum a) => a -> a
tan' x = sin' x / cos' x
