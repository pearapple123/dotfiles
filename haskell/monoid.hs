import Data.Semigroup hiding (Sum)
import Data.Monoid hiding (Sum)
import GHC.Prim (coerce)

newtype Sum a = Sum { getSum :: a }
  deriving (Eq,Ord)

instance Num a => Semigroup (Sum a) where
  (<>) = coerce ((+) :: a -> a -> a)

instance Num a => Monoid (Sum a) where
  mempty = Sum 0
