import Control.Monad.State
import Control.Monad.Random.Class (getRandomR,MonadRandom)

playGame :: IO ()
playGame = do
  num <- getRandomR (1,100)
  input <- getLine
  if (read input :: Int) == num then return ()
    else playGame

playGameWithGuesses :: IO Int
playGameWithGuesses = do
  num <- (getRandomR (1,100) :: MonadRandom m => m Int)
  aux num 0
    where
      aux fin guess = do
        input <- getLine
        if (read input) == fin then return $ succ guess
          else aux fin $ succ guess

playGameWithState :: IO ()
playGameWithState = do
  num <- (getRandomR (1,10) :: MonadRandom m => m Int)
  runStateT (aux num) 0 >>= print
    where
      aux fin = do
        count <- get
        input <- liftIO getLine
        put $ succ count
        if (read input) == fin then return $ read input
          else aux fin

main = return ()
