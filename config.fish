set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $fish_user_paths

set fish_greeting
set fish_prompt_pwd_dir_length 0
set EDITOR "nvim"
set TERM "xterm-256color"
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

#vi mode
function fish_user_key_bindings
    fish_vi_key_bindings
end

#Prompt
function fish_prompt
  printf '%s%s%s λ ' \
    (set_color $fish_color_cwd) (prompt_pwd) (set_color normal)
end

function fish_mode_prompt; end
funcsave fish_mode_prompt

#Aliases
alias ls='exa -lha'
alias cat='bat'
alias grep='grep --color=auto'
alias apg='apg -n 1 -m 16 -x 24 -a 0 -s'
alias vim='nvim'

alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

alias syu="sudo pacman -Syu"
alias orph="sudo pacman -Rns (pacman -Qtdq)"

#starship init fish | source
