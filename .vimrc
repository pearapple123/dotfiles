set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Vundle plugins go here
Plugin 'morhetz/gruvbox'
call vundle#end()
filetype plugin indent on
autocmd vimenter * ++nested colorscheme gruvbox
set background=dark
syntax on
