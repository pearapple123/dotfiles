set expandtab
set tabstop=4
set shiftwidth=4

let g:airline_powerline_fonts = 1
let g:airline_theme='wombat'

map <C-F> :NERDTreeToggle<CR>
map <C-O> :tabn<CR>
map <S-T> :FloatermNew --position=bottom --height=0.25 --width=1.0 --autoclose=2<CR>

au FileType haskell 
            \ set tabstop=2 |
            \ set shiftwidth=2

call plug#begin('~/.config/nvim/plugins')
Plug 'morhetz/gruvbox'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jiangmiao/auto-pairs', {'branch': 'master'}
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'voldikss/vim-floaterm'
call plug#end()

"autocmd vimenter * ++nested colorscheme gruvbox
colorscheme monokai
syntax on
highlight Comment cterm=italic
